<?php

if ( class_exists( 'FLUpdater' ) ) {
	FLUpdater::add_product( array(
		'name'    => 'Beaver Themer',
		'version' => '1.4.4',
		'slug'    => 'bb-theme-builder',
		'type'    => 'plugin',
	) );
}
